import uproot3 as uproot
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style(style = "ticks")
sns.set_palette(sns.color_palette("colorblind"))
sns.set_color_codes()
matplotlib.rcParams['figure.dpi']= 100
from matplotlib.backends.backend_pdf import PdfPages

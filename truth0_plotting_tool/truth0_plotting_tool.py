import uproot3 as uproot
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import seaborn as sns
sns.set_style(style = "ticks")
sns.set_palette(sns.color_palette("colorblind"))
sns.set_color_codes()
matplotlib.rcParams['figure.dpi']= 100
from matplotlib.backends.backend_pdf import PdfPages


def multipage(filename, figs=None, dpi=200):
    """Creates a pdf with one page per plot"""
    pp = PdfPages(filename)
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        plt.figure(fig).savefig(pp, format='pdf')
    pp.close()
    
def get_normalization(y,bin_edges):
    """Integrate over variable size bins"""
    integral = 0
    for i in range(len(y)):
        dx = bin_edges[i+1]-bin_edges[i]
        integral += dx*y[i]
    return integral

def create_title(my_title):
    """Create title for histogram from truth0 reader root keys"""
    title = ""
    title = r"$"+str(my_title.decode("utf-8"))+"$" # Get key
    title = title.replace("#","\\") # Because not markdown
    # Add units where needed
    if "p_{T}" in title:
        title += " [GeV]"
    elif "m" in title:
        title += " [GeV]"
    elif "E" in title:
        title += " [GeV]"
    if "\DeltaR" in title:
        title = title.replace("\DeltaR","\Delta R")
    if "Number of q" in title:
        title = "Number of VBF quarks"
    if "Number of jets" in title:
        title = "Number of Jets"
    if "\nuu" in title:
        title = title.replace("\nuu","\nu")
    if "vs weight" in title:
        title = title.replace("vs weight","")
    print(title)
    return title

# def plot_histogram(file,key,c2v,heading):
#     """ Make plots """
#     y, bin_edges = file.numpy()
#     bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    
#     # Normalize histogram inputs
#     normalization = get_normalization(y,bin_edges)
#     y = y/normalization

#     # Make the plot
#     plt.errorbar(
#                 bin_centers,
#                 y,
#                 drawstyle = 'steps-mid',
#                 linewidth=2,
#                 label = r" $c_{2v}$ = "+str(c2v)
#                 )
#     title = create_title(file.title)
#     plt.title(heading+": $\kappa_{\lambda}$ = 1, $c_v$ = 1")
#     plt.xlabel(title)
#     plt.ylabel("Normalized Weighted events")
#     plt.legend(title="",frameon=False)


# def plot_histogram(file,key,kappa_lambda):
#     """ Make plots """
#     y, bin_edges = file.numpy()
#     bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    
#     # Normalize histogram inputs
#     normalization = get_normalization(y,bin_edges)
#     y = y/normalization

#     # Make the plot
#     plt.errorbar(
#                 bin_centers,
#                 y,
#                 drawstyle = 'steps-mid',
#                 linewidth=2,
#                 label = r"$\kappa_{\lambda} = "+str(kappa_lambda)+"$"
#                 )
#     title = create_title(file.title)
#     plt.xlabel(title)
#     plt.ylabel("Normalized Weighted events")
#     plt.legend(title="",frameon=False)


def plot_histogram(file,key, heading="", label=None):
    """ Make 1D histograms """

    y, bin_edges = file.numpy()
    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
    
    # Normalize histogram inputs
    normalization = get_normalization(y,bin_edges)
    y = y/normalization

    # Make the plot
    plt.errorbar(
                bin_centers,
                y,
                drawstyle = 'steps-mid',
                linewidth=2,
                label = label
                )
    title = create_title(file.title)
    plt.title(heading)#+": $\kappa_{\lambda}$ = 1, $c_v$ = 1, $c_vv$ = 1")
    plt.xlabel(title)
    plt.ylabel("Normalized Weighted events")
    if label is not None:
        plt.legend(bbox_to_anchor=(1.6, 1),title="",frameon=False)
        
def plot_2d_histogram(file,key,c2v,heading="", label=None):
    """ Make 2D histograms """
    z, bins = file.numpy()
    x_nc,y_nc = bins[0][0],bins[0][1] #uncentered bins
    xb = (x_nc[1:]+x_nc[:-1])/2 #centered bins
    yb = (y_nc[1:]+y_nc[:-1])/2 #centered bins

    
    z_min, z_max = 0, np.abs(z).max()
    empty = np.equal(z,0)
    z[empty] = np.nan # mask out 0 values
    fig, ax = plt.subplots(num=str(key)+": "+str(c2v))
    c = ax.pcolormesh( xb, yb, z.T, cmap='viridis', vmin=z_min, vmax=z_max)
    fig.colorbar(c,ax=ax,label="Events")
    plt.xlabel(create_title(file.title))
    plt.ylabel("Weight")
    plt.title(heading+" "+label)#+": $\kappa_{\lambda}$ = 1, $c_v$ = 1, $c_vv$ = 1")
    

        
def make_all_plots(my_dict, heading, save_name):
    """ Makes validation plots for scans """
    for value in my_dict.keys(): 

        # VBF
        file_name_bbyy   = "../data/job-jj-bbyy-"+value+"/hist-"+value+".root"
        # Non-VBF Plots
        file_name2_bbyy   = "../data/job2-jj-bbyy-"+value+"/hist-"+value+".root"

        file_bbyy =   uproot.open(file_name_bbyy)
        file2_bbyy =   uproot.open(file_name2_bbyy)

        keys = file_bbyy.keys()
        keys_bbyy = file2_bbyy.keys()
        keys_bbyy =[x for x in keys_bbyy if x not in keys]

        #print(keys)
        #print(keys_bbyy)

        # Plot a histogram for each saved TH1F
        for key in keys:
            if file_bbyy[key].classname == 'TH1F':
                plt.figure(str(key))        
                plot_histogram(file_bbyy[key],key,heading,my_dict[value])
                #ymin, ymax = plt.ylim() # return the current ylim
                if "phi" in str(key): # don't zoom in on phi
                    plt.ylim(bottom=0,top=0.25)
            #if file_bbyy[key].classname == 'TH2F':
            #    print(value)
            #    print(my_dict[value]) 
            #    plot_2d_histogram(file_bbyy[key],key,value,heading,my_dict[value])
                

        for key in keys_bbyy:
            if file2_bbyy[key].classname == 'TH1F':
                plt.figure(str(key))        
                plot_histogram(file2_bbyy[key],key,heading,my_dict[value])
                #ymin, ymax = plt.ylim() # return the current ylim
                #plt.ylim((0, ymax))
                if "phi" in str(key): # don't zoom in on phi
                    plt.ylim(bottom=0,top=0.25)

        # Save output to pdf 
        #multipage("../plots/"+save_name+"_bbyy_13tev_weights.pdf", figs=plt.get_fignums(), dpi=100)
        multipage("../plots/"+save_name+"_bbyy_13tev.pdf", figs=plt.get_fignums(), dpi=200)

def make_vbf_plots(my_dict, heading, save_name):
    """ Makes validation plots for scans """
    for value in my_dict.keys(): 

        # VBF
        file_name_bbyy   = "../data/job-jj-bbyy-"+value+"/hist-"+value+".root"


        file_bbyy =   uproot.open(file_name_bbyy)


        keys = file_bbyy.keys()

        print(keys)
        #print(keys_bbyy)

        # Plot a histogram for each saved TH1F
        for key in [b'qq_m;1', b'jj_m;1',b'qq_pT;1',b'jj_pT;1', b'dEta_qq;1',b'dEta_jj;1',b'Num_q;1',b'Num_j;1']:
            if file_bbyy[key].classname == 'TH1F':
                plt.figure(str(key))        
                plot_histogram(file_bbyy[key],key,heading,my_dict[value])
                #ymin, ymax = plt.ylim() # return the current ylim
                if "phi" in str(key): # don't zoom in on phi
                    plt.ylim(bottom=0,top=0.25)
            #if file_bbyy[key].classname == 'TH2F':
            #    print(value)
            #    print(my_dict[value]) 
            #    plot_2d_histogram(file_bbyy[key],key,value,heading,my_dict[value])
                
    multipage("../plots/"+save_name+"_bbyy_13tev.pdf", figs=plt.get_fignums(), dpi=200)

def make_all_plots2d(my_dict, heading, save_name):
    """ Makes validation plots for scans """
    for value in my_dict.keys(): 

        # VBF
        file_name_bbyy   = "../data/job-bbyy-"+value+"/hist-"+value+".root"
        # Non-VBF Plots
        file_name2_bbyy   = "../data/job2-bbyy-"+value+"/hist-"+value+".root"

        file_bbyy =   uproot.open(file_name_bbyy)
        file2_bbyy =   uproot.open(file_name2_bbyy)

        keys = file_bbyy.keys()
        keys_bbyy = file2_bbyy.keys()
        keys_bbyy =[x for x in keys_bbyy if x not in keys]

        #print(keys)
        #print(keys_bbyy)

        # Plot a histogram for each saved TH1F
        for key in keys:
            # if file_bbyy[key].classname == 'TH1F':
            #     plt.figure(str(key))        
            #     plot_histogram(file_bbyy[key],key,heading,my_dict[value])
            #     #ymin, ymax = plt.ylim() # return the current ylim
            #     if "phi" in str(key): # don't zoom in on phi
            #         plt.ylim(bottom=0,top=0.25)
            if file_bbyy[key].classname == 'TH2F':
               print(value)
               print(my_dict[value]) 
               plot_2d_histogram(file_bbyy[key],key,value,heading,my_dict[value])
                

        # for key in keys_bbyy:
        #     if file2_bbyy[key].classname == 'TH1F':
        #         plt.figure(str(key))        
        #         plot_histogram(file2_bbyy[key],key,heading,my_dict[value])
        #         #ymin, ymax = plt.ylim() # return the current ylim
        #         #plt.ylim((0, ymax))
        #         if "phi" in str(key): # don't zoom in on phi
        #             plt.ylim(bottom=0,top=0.25)

        # Save output to pdf 
        multipage("../plots/"+save_name+"_bbyy_13tev_weights.pdf", figs=plt.get_fignums(), dpi=100)
        #multipage("../plots/"+save_name+"_bbyy_13tev.pdf", figs=plt.get_fignums(), dpi=200)



def plot_histogram_w_ratio(my_dict, heading, path):

    """ Makes validation plots for scans """
    assert len(my_dict.keys())==2, "Error, need exactly two files in dict" 
    """ Make 1D histograms with error bars"""
    job0 = list(my_dict.keys())[0]
    job1 = list(my_dict.keys())[1]
    # VBF
    file_name_bbyy_0   = "../data/"+path+job0+"/hist-"+job0+".root"
    file_name_bbyy_1   = "../data/"+path+job1+"/hist-"+job1+".root"
    # Non-VBF Plots
    #file_name2_bbyy   = "../data/job2-bbyy-"+value+"/hist-"+value+".root"

    file_bbyy_0 =   uproot.open(file_name_bbyy_0)
    file_bbyy_1 =   uproot.open(file_name_bbyy_1)
    #file2_bbyy =   uproot.open(file_name2_bbyy)

    keys = file_bbyy_0.keys()
    #return file_bbyy_0
    #keys_bbyy = file2_bbyy.keys()
    #keys_bbyy =[x for x in keys_bbyy if x not in keys]
    

    for key in keys:
        if file_bbyy_0[key].classname == 'TH1F':
            # Create the upper plot and the ratio plot
            f, (a0, a1) = plt.subplots(2,1,gridspec_kw = {'height_ratios':[3, 1]},figsize=(8,6))

            file0 = file_bbyy_0[key]
            file1 = file_bbyy_1[key]
            y1, bin_edges = file0.numpy()
            y2, bin_edges = file1.numpy()
            bin_centers = bin_edges[:-1]
            #bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
            #bin_centers2 = bin_edges[1:]#0.5*(bin_edges[1:] + bin_edges[:-1])

            # Normalize histogram inputs
            normalization1 = get_normalization(y1,bin_edges)
            normalization2 = get_normalization(y2,bin_edges)
            
             
            color1 = "#0173B2"
            color2 = "#DE8F05"
            # if heading=="10":
            #     color1 = "#029E73"
            #     color2 =  "#D55E00"
            
            y1 = y1/normalization1
            y2 = y2/normalization2
            #print(y.variances)

            # Make the main plot
            a0.errorbar(
                        bin_centers,
                        y1,
                        #drawstyle = 'steps-post',
                        drawstyle = 'steps-mid',
                        linewidth=2,
                        label = my_dict[job0],
                        color=color1
                        )
            a0.fill_between(bin_centers, y1-np.sqrt(file0.variances)/normalization1, 
                             y1+np.sqrt(file0.variances)/normalization1, 
                            step='mid',
                            #step='post',
                            alpha=0.5,
                            color=color1,
                            linewidth=0)
            
            a0.errorbar(
                        bin_centers,
                        y2,
                        #drawstyle = 'steps-post',
                        drawstyle = 'steps-mid',
                        linewidth=2,
                        label = my_dict[job1],
                color=color2
                        )
            a0.fill_between(bin_centers, y2-np.sqrt(file1.variances)/normalization2, 
                             y2+np.sqrt(file1.variances)/normalization2, 
                            step='mid',
                            #step='post',
                            alpha=0.5,linewidth=0,color=color2)
            a0.set_ylabel("Normalized Weighted Events")
            a0.legend(frameon=False)
            y_min, y_max = a0.get_ylim()
            a0.set_ylim([0,y_max])
            a0.set_title(heading)
            # Make the ratio plot
            ratio = y1/y2
            undefined = np.logical_or(np.isnan(ratio),np.isinf(ratio))
            ratio[undefined] = 0.0
            ratio_errors = ratio*np.sqrt(np.power(np.sqrt(file0.variances)/normalization1/y1,2)+np.power(np.sqrt(file1.variances)/normalization2/y2,2))
            ratio_errors[undefined] = 0.0
            
            #print(ratio)
            a1.errorbar(
                        bin_centers,
                        ratio,
                        xerr=(bin_centers[1]-bin_centers[0])/2.0,
                        fmt='.',
                        color=color1,
                        linestyle="None",
                        #drawstyle = 'steps-post',
                        #drawstyle = 'steps-mid',
                        #linewidth=2,
                        #label = label
                
                        )
            


            a1.fill_between(bin_centers, ratio-ratio_errors, 
                             ratio+ratio_errors, 
                            #step='pre',
                            step='mid',
                            color=color1,
                            #step='post',
                            alpha=0.5,linewidth=0)
            a1.plot([bin_edges[0],bin_edges[-1]],[1,1],':',color='gray')
            
            a0.set_xlim(plt.xlim())#[bin_edges[0],bin_edges[-1]])
            a1.set_xlim(plt.xlim())#[bin_edges[0],bin_edges[-1]])
            
            a1.set_ylim([0.5,1.5])

            a1.set_ylabel("Ratio")
            

            #plt.title(heading)#+": $\kappa_{\lambda}$ = 1, $c_v$ = 1, $c_vv$ = 1")
            title = create_title(file0.title)
            plt.xlabel(title)
            #f.tight_layout()
            
            #if label is not None:
            #    plt.legend(title="",frameon=False)

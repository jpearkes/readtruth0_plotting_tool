# readtruth0_plotting_tool

Makes hh byyy validation plots from the truth0 reader tool and saves them as a pdf.

Truth reader tool is here: https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_mc_truth0/